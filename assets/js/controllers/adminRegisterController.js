/**
 * Created by rbn on 6/7/15.
 */
angular.module('app')
    .controller('AdminRegisterController', function ($scope,Auth,$state) {
        //console.log("driver login");
        $scope.admin = {};
        $scope.adminRegister = function (cred) {
            Auth.register(cred).success(function (result) {
                console.log('registration return',result);
                $state.go('admin.dashboard');
            }).error(function (err) {
                alert('Registration Error');
                //console.log("login failed");
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        };
    });
