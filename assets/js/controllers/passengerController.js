/**
 * Created by rbn on 6/9/15.
 */
angular.module('app')
    .controller('PassengerController', function ($scope, Auth, $state,Passenger) {
        console.log("Passenger controller");
        $scope.passengers = {};
        Passenger.all().success(function(response){
            //console.log('all driver',response);
            $scope.passengers = response;
        }).error(function(err){
            console.log('error driver loading',err);
        });
    });