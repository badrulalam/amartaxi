/**
 * Created by rbn on 6/9/15.
 */
angular.module('app')
    .controller('BookingController', function ($scope, Auth, $state, Booking) {
        console.log("dashboard controller");
        $scope.bookings = {};
        Booking.all().success(function(response){
            console.log('Booking controller',response);
            $scope.bookings = response;
        }).error(function(err){
            console.log('error Booking loading',err);
        });
        //
        //$scope.tableParams = new ngTableParams({
        //    page: 1,            // show first page
        //    count: 10           // count per page
        //}
        //    , {
        //    total: bookings.length,     // length of data
        //    getBooking: function($defer, params) {
        //        $defer.resolve(bookings.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        //    }
        //}
        //);
    });