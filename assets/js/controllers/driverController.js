/**
 * Created by rbn on 6/9/15.
 */
angular.module('app')
    .run(function(editableOptions) {
        editableOptions.theme = 'bs3';
    })
    .controller('DriverController', function ($scope,  $filter, $http, Auth, $state, Driver) {
        console.log("Driver controller");
        $scope.drivers = {};

        //$scope.statuses = [
        //    {value: 1, text: 'status1'},
        //    {value: 2, text: 'status2'},
        //    {value: 3, text: 'status3'},
        //    {value: 4, text: 'status4'}
        //];
        //
        //$scope.groups = [];
        //$scope.loadGroups = function() {
        //    return $scope.groups.length ? null : $http.get('/groups').success(function(data) {
        //        $scope.groups = data;
        //    });
        //};

        Driver.all().success(function(response){
            //console.log('all driver',response);
            $scope.drivers = response;
        }).error(function(err){
            console.log('error driver loading',err);
        });

        $scope.saveUser = function(data, id) {
            console.log('data', data);
            console.log('id', id);
            Driver.updateById(id,data).success(function(result){
                console.log('successfully updated',result);
            });
        };

        // remove user
        $scope.removeUser = function(index) {
            $scope.Driver.splice(index, 1);
        };
    });
