/**
 * Created by rbn on 6/7/15.
 */
angular.module('app')
    .controller('AdminLoginController', function ($scope,Auth,$state) {
        //console.log("driver login");
        $scope.admin = {};
        $scope.adminLogin = function (cred) {
            Auth.login(cred).success(function (result) {
                console.log('login return',result);
                $state.go('admin.dashboard');
            }).error(function (err) {
                alert('Error login');
                //console.log("login failed");
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        };
    });
