angular.module('app')
    .controller('MenuController', function ($scope, Auth, $state) {

        $scope.status = [];
        for(var i=0;i<50;i++) {
            $scope.status[i] = {
                isopen: false
            };
        };
        $scope.toggled = function(open) {
            console.log('Dropdown is now: ', open);
        };

        $scope.toggleDropdown = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };
        console.log("Menu controller");
        jQuery(function(){ // on DOM load

            menu1 = new pushmenu({  // initialize menu example
                menuid: 'pushmenu1',
                position: 'left',
                marginoffset: 0,
                revealamt: -8,
                onopenclose:function(state){ // add or remove "open" class to animated drawer button depending on menu state
                    var $buttonref = $('#drawer')
                    if (state == 'open')
                        $buttonref.addClass('open')
                    else
                        $buttonref.removeClass('open')
                }
            })

        })
    });