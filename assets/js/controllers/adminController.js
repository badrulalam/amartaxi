/**
 * Created by rbn on 6/8/15.
 */
angular.module('app')
    .controller('AdminController', function($scope,Auth,$state,CurrentUser) {
        console.log("admin controller");
        $scope.signOut = function(){
            Auth.logout();
            $state.go('adminlogin');
        };

        if(!Auth.isAuthenticated()){
            $state.go('adminlogin');
        }
        else{
            $scope.adminInfo = JSON.parse(Auth.isAuthenticated());
            //console.log('authenticated',$scope.adminInfo);
        }
    });