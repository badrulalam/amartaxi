var app = angular.module('app', ['ui.router','xeditable','pascalprecht.translate','ui.bootstrap','ui.grid','duScroll','ngTable','ui.select','leaflet-directive','angularFileUpload','ngSanitize','ngCsv'])
  .run(function($rootScope, $state, Auth, $location, $window) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

      if(toState.hasOwnProperty('data')){

        if (!Auth.authorize(toState.data.access)) {
          event.preventDefault();

          $state.go('login');
        }
      }


    });

     $rootScope
    .$on('$stateChangeSuccess',
        function(event){

            if (!$window.ga)
                return;

            $window.ga('send', 'pageview', { page: $location.path() });
    });
  });
