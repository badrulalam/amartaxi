/**
 * Created by rbn on 6/10/15.
 */
angular.module('app')
.factory('Driver', function ($http, CurrentUser) {
    return {
        all: function () {
            var drivers = $http.get('/driver').success(function (resp) {
                console.log('Success - All Driver: ', resp);
            }, function (err) {
                console.log('Error - All Driver ', err);
            });
            return drivers;
        },
        updateById: function (id,data) {
            var driver = $http.post('/driver/update/'+id,data).success(function (resp) {
                console.log('Success update - All Driver: ', resp);
            }, function (err) {
                console.log('Error - All Driver ', err);
            });
            return driver;
        }
    };
});
