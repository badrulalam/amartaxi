/**
 * Created by rbn on 6/10/15.
 */
angular.module('app')
    .factory('Booking', function ($http, CurrentUser) {
        return {
            all: function () {
                var bookings = $http.get('/booking').success(function (resp) {
                    console.log('Success - All Bookings: ', resp);
                }, function (err) {
                    console.log('Error - All Bookings ', err);
                });
                return bookings;
            }
        };
    });
