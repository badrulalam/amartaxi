/**
 * Created by rbn on 6/10/15.
 */
angular.module('app')
    .factory('Passenger', function ($http, CurrentUser) {
        return {
            all: function () {
                var passengers = $http.get('/user').success(function (resp) {
                    console.log('Success - All Passenger: ', resp);
                }, function (err) {
                    console.log('Error - All Passenger ', err);
                });
                return passengers;
            }
        };
    });
