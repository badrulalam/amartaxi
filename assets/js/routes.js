angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider, AccessLevels) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/templates/home.html',
                controller: 'HomeController',
                data: {
                    access: AccessLevels.anon
                }
            })
            .state('about_us', {
                url: '/about-us',
                templateUrl: '/templates/about_us.html',
                controller: 'AboutUsController'
            })
            .state('contact_us', {
                url: '/contact-us',
                templateUrl: '/templates/contact_us.html',
                controller: 'ContactUsController'
            })
            .state('benefits_driver', {
                url: '/benefits-driver',
                templateUrl: '/templates/benefits_driver.html',
                controller: 'BenefitsDriverController'
            })
            .state('benefits_owner', {
                url: '/benefits-owner',
                templateUrl: '/templates/benefits_owner.html',
                controller: 'BenefitsOwnerController'
            })
            .state('benefits_passenger', {
                url: '/benefits-passenger',
                templateUrl: '/templates/benefits_passenger.html',
                controller: 'BenefitsPassengerController'
            })
            .state('faq', {
                url: '/faq',
                templateUrl: '/templates/faq.html',
                controller: 'FaqController'
            })
            .state('license', {
                url: '/license',
                templateUrl: '/templates/license.html',
                controller: 'LicenseController'
            })
            .state('adminlogin', {
                url: '/admin',
                templateUrl: '/templates/admin/admin-login.html',
                controller: 'AdminLoginController'
            })
            .state('adminregister', {
                url: '/register',
                templateUrl: '/templates/admin/admin-register.html',
                controller: 'AdminRegisterController'
            });
        //.state('dashboard', {
        //    url: '/dashboard',
        //    templateUrl: '/templates/admin/dashboard.html',
        //    controller: 'DashboardController'
        //});

        $stateProvider
            .state('admin', {
                url: '/admin',
                abstract: true,
                templateUrl: '/templates/admin/menu.html',
                controller: 'AdminController',
                data: {
                    access: AccessLevels.admin
                }
            })
            .state('admin.dashboard', {
                url: '/dashboard',
                templateUrl: '/templates/admin/dashboard.html',
                controller: 'DashboardController'
            })
            .state('admin.driver', {
                url: '/driver',
                templateUrl: '/templates/admin/driver.html',
                controller: 'DriverController'
            })
            .state('admin.passenger', {
                url: '/passenger',
                templateUrl: '/templates/admin/passenger.html',
                controller: 'PassengerController'
            })
            .state('admin.booking', {
                url: '/booking',
                templateUrl: '/templates/admin/booking.html',
                controller: 'BookingController'
            })
            .state('admin.tracking', {
                url: '/tracking',
                templateUrl: '/templates/admin/tracking.html',
                controller: 'TrackingController'
            });
        //.state('dashboard', {
        //    url: '/dashboard',
        //    templateUrl: '/templates/admin/dashboard.html',
        //    controller: 'DashboardController'
        //})

        $urlRouterProvider.otherwise('/');

    });