/**
 * DriverController
 *
 * @description :: Server-side logic for managing Drivers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    // Doing a DELETE /user/:parentid/message/:id will not delete the message itself
    // We do that here.
    remove: function(req, res) {
        var relation = req.options.alias;
        switch (relation) {
            case 'messages':
                destroyMessage(req, res);
        }
    },
    create: function(req, res) {
        //res.json(301, 'To create a user go to /driverauth/register');

        res.json(301, 'To create a user go to /driverauth/register');
    },
    saveCurrentPosition: function(req, res) {

      console.log(req.body);
      //  res.json(301, 'To create a user go to /driverauth/register');
      // var user = Driver.findOne(req.body.id).done(function(error, user) {
      //       if(error) {
      //           // do something with the error.
      //       }
      //
      //       // if(req.body.email) {
      //       //     // validate whether the email address is valid?
      //       //
      //       //     // Then save it to the object.
      //       //     user.email = req.body.email;
      //       // }
      //       // // Repeat for each eligible attribute, etc.
      //       if(req.body.lat){
      //         user.current_lat = req.body.lat;
      //       }
      //       if(req.body.long){
      //         user.current_long = req.body.long;
      //       }
      //       if(req.body.status){
      //         user.status = req.body.status;
      //       }
      //
      //       user.save(function(error) {
      //           if(error) {
      //               // do something with the error.
      //           } else {
      //               // value saved!
      //               req.send(user);
      //           }
      //       });
      //   });
      Driver.update({id: req.body.id}, {current_lat: req.body.lat,current_long: req.body.long})
       .exec(function(err, updatedServers) {
         if (err) {
           res.json(err.status, {err: err});
           return;
         }
         if (updatedServers) {
           res.json(301, 'change success');
         }
       });
    }
};
