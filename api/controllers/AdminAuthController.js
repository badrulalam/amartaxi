/**
 * AdminAuthController
 *
 * @description :: Server-side logic for managing Adminauths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    authenticate: function(req, res) {
        var email = req.param('email');
        var password = req.param('password');

        if (!email || !password) {
            return res.json(401, {err: 'email and password required'});
        }

        Admin.findOneByEmail(email, function(err, admin) {
            if (!admin) {
                return res.json(401, {err: 'invalid email or password'});
            }

            Admin.validPassword(password, admin, function(err, valid) {
                if (err) {
                    return res.json(403, {err: 'forbidden'});
                }

                if (!valid) {
                    return res.json(401, {err: 'invalid email or password'});
                } else {
                    res.json({admin: admin, token: sailsTokenAuth.issueToken({sid: admin.id})});
                }
            });
        })
    },
    register: function(req, res) { console.log(req.body);
        //TODO: Do some validation on the input
        if (req.body.password !== req.body.confirmPassword) {
            return res.json(401, {err: 'Password doesn\'t match'});
        }

        Admin.create({email: req.body.email,name:req.body.name,mobile:req.body.mobile, password: req.body.password,
            jid: req.body.jid,
            jpassword: req.body.jpassword }).exec(function(err, admin) {
            if (err) {
                res.json(err.status, {err: err});
                return;
            }
            if (admin) {
                res.json({admin: admin, token: sailsTokenAuth.issueToken({sid: admin.id})});
            }
        });
    }
};

