/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var connectionData = {
    host: 'amartaxi.com',
    user: 'ejabberd',
    password: 'ejabberd',
    database: 'ejabberd'
};
module.exports = {
    authenticate: function(req, res) {
        var mobile = req.param('mobile');
        var password = req.param('password');

        if (!mobile || !password) {
            return res.json(401, {err: 'mobile number and password required'});
        }

        Driver.findOneByMobile(mobile, function(err, driver) {
            if (!driver) {
                return res.json(401, {err: 'invalid mobile number or password'});
            }

            Driver.validPassword(password, driver, function(err, valid) {
                if (err) {
                    return res.json(403, {err: 'forbidden'});
                }

                if (!valid) {
                    return res.json(401, {err: 'invalid mobile number or password'});
                } else {
                    res.json({driver: driver, token: sailsTokenAuth.issueToken({sid: driver.id})});
                }
            });
        })
    },

    register: function(req, res) {
        console.log("what");
        console.log(req.body);
        //TODO: Do some validation on the input
        if (req.body.password !== req.body.confirmPassword) {
            return res.json(401, {err: 'Password doesn\'t match'});
        }
        //***************************************************
    		var mysql      = require('mysql');
    		var connection = mysql.createConnection(connectionData);


        User.findOneByMobile(req.body.mobile , function(err, find_user){
            if(find_user)
            {
                console.log("find_user found",find_user);
                return res.json(401, {err: 'mobile found on passenger'});
            }
            else{

                Driver.findOneByMobile(req.body.mobile , function(err2, find_driver){
                    if(find_driver)
                    {
                        console.log("driver found",find_driver);
                        return res.json(401, {err: 'mobile found on driver'});

                    }
                    else{
                        console.log("else false driver", find_driver);
                        //create code goes here
                        connection.connect();
                        var querySQL = 'INSERT INTO `users`(`username`, `password`) VALUES ("' + req.body.mobile + '","' + req.body.jpassword + '")';
                        connection.query(querySQL, function (mysql_err, rows, fields) {
                            if (!mysql_err)
                            {
                                Driver.create({driverId: req.body.driverId, car_type: req.body.car_type,email: req.body.email,name:req.body.name,mobile:req.body.mobile, password: req.body.password,jid: req.body.mobile,
                                    jpassword: req.body.jpassword }).exec(function(err, driver) {
                                    if (err) {
                                        console.log("ola");
                                        res.json(err.status, {err: err});
                                        return;
                                    }
                                    if (driver) {
                                        res.json({driver: driver, token: sailsTokenAuth.issueToken({sid: driver.id})});
                                    }

                                });
                            }
                            else
                                res.json(401,mysql_err);

                        });
                        connection.end();
                        //create code end

                    }

                });
            }

        });


    }
};
