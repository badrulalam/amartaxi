/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var connectionData = {
    host: 'amartaxi.com',
    user: 'ejabberd',
    password: 'ejabberd',
    database: 'ejabberd'
};
module.exports = {
    updatepassword: function(req, res){
      console.log(req.body);
        var curPass = req.param('currentPass');
        var newPass = req.param('oldPass');
        var conPass = req.param('confirmCurPass');
        var uid = req.param('id');
    },

    update: function (req, res) {
        console.log(req.body);
        var uname = req.param('name');
        var uemail = req.param('email');
        var umobile = req.param('mobile');
        var uid = req.param('id');
        console.log(uname, uemail, umobile, uid);
        User.update({id: uid}, {name: uname, email: uemail, mobile: umobile}).exec(function (err, updatedRecords) {
            if (err) {
                return res.send({message: 'Could not update the records', err: err}, 500);
            }
            if (updatedRecords) {
                return res.send({records: updatedRecords}, 200);
            } else {
                return res.notFound('Records not found');
            }
        });
    },

    authenticate: function (req, res) {
        var mobile = req.param('mobile');
        var password = req.param('password');

        if (!mobile || !password) {
            return res.json(401, {err: 'mobile number and password required'});
        }
        User.findOneByMobile(mobile, function (err, user) {
            if (!user) {
                return res.json(401, {err: 'invalid mobile number or password'});
            }

            User.validPassword(password, user, function (err, valid) {
                if (err) {
                    return res.json(403, {err: 'forbidden'});
                }

                if (!valid) {
                    return res.json(401, {err: 'invalid mobile number or password'});
                } else {
                    res.json({user: user, token: sailsTokenAuth.issueToken({sid: user.id})});
                }
            });
        })
    },

    register: function (req, res) {
        console.log(req.body);
        //TODO: Do some validation on the input
        if (req.body.password !== req.body.confirmPassword) {
            return res.json(401, {err: 'Password doesn\'t match'});
        }
        //***************************************************
        var mysql = require('mysql');
        var connection = mysql.createConnection(connectionData);


        User.findOneByMobile(req.body.mobile , function(err, find_user){
            if(find_user)
            {
                console.log("find_user found",find_user);
                return res.json(401, {err: 'mobile found on passenger'});
            }
            else{

                Driver.findOneByMobile(req.body.mobile , function(err2, driver){
                    if(driver)
                    {
                        console.log("driver found",driver);
                        return res.json(401, {err: 'mobile found on driver'});

                    }
                    else{
                        console.log("else false driver", driver);
                        //create code goes here
                        connection.connect();
                        var querySQL = 'INSERT INTO `users`(`username`, `password`) VALUES ("' + req.body.mobile + '","' + req.body.jpassword + '")';
                        connection.query(querySQL, function (mysql_err, rows, fields) {
                            if (!mysql_err)
                            {
                                User.create({
                                    email: req.body.email, name: req.body.name, mobile: req.body.mobile, password: req.body.password,
                                    jid: req.body.mobile,
                                    jpassword: req.body.jpassword
                                }).exec(function (user_create_err, user) {
                                    if (user_create_err) {
                                        res.json(err.status, {err: user_create_err});
                                        return;
                                    }
                                    if (user) {
                                        res.json({user: user, token: sailsTokenAuth.issueToken({sid: user.id})});
                                    }
                                });
                            }
                            else
                                res.json(401,mysql_err);

                        });
                        connection.end();
                        //create code end
                    }
                });
            }
        });
    },
    test: function (req, res) {
        var mysql = require('mysql');
        var connection = mysql.createConnection(connectionData);

            var querySQL = 'SELECT * FROM `users`';
            console.log(querySQL );
            connection.query(querySQL, function (err, rows, fields) {
                //console.log("err", err);
                //console.log("rows", rows);
                //console.log("fields", fields);

                if (!err) {
                    res.json(rows);
                    //rows.push(rows);
                    //console.log(rows);
                }else {
                     res.json("error");
                    console.log(err);
                }
            });


        connection.end();

        var q = require('q');

        var index = 1;

        var useless =  function(){
            var currentIndex = index;
            console.log(currentIndex);
            var deferred = q.defer();
            setTimeout(function(){
                if(currentIndex > 10)
                    deferred.resolve(false);
                else deferred.resolve(true);
            },500);
            return deferred.promise;
        };

        var control = function(cont){
            var deferred = q.defer();
            if(cont){
                index = index + 1;
                useless().then(control).then(function(){
                    deferred.resolve();
                });
            }
            else deferred.resolve();
            return deferred.promise;
        };

        var chain = useless().then(control).then(function(){console.log('done')});
    }
};
