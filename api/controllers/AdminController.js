/**
 * AdminController
 *
 * @description :: Server-side logic for managing Admins
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */


module.exports = {
    // Doing a DELETE /admin/:parentid/message/:id will not delete the message itself
    // We do that here.
    remove: function(req, res) {
        var relation = req.options.alias;
        switch (relation) {
            case 'messages':
                destroyMessage(req, res);
        }
    },

    create: function(req, res) {
        res.json(301, 'To create a admin go to /adminauth/register');
    }
};