/**
 * BookingController
 *
 * @description :: Server-side logic for managing Bookings
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    setByPassenger: function (req, res) {
        User.findOne()
            .where({id: req.body.passenger})
            .exec(function (err, response) {
                if(response)
                {
                    if (response.hasOwnProperty('current_booking_id') && response.current_booking_id != "") {
                        console.log("booking id found", typeof response.current_booking_id);
                        var booking_id = response.current_booking_id;
                        Booking.update({id: booking_id}, req.body)
                            .exec(function (err, response) {
                                if (!err)
                                    return res.json(response[0]);
                                else
                                    return res.json(404, err);
                            });
                    } else {
                        console.log("booking id not found");
                        Booking.create(req.body)
                            .then(function (data) {
                                return User.update({id: req.body.passenger}, {current_booking_id: data.id});
                            }).then(function (val2) {
                                return Booking.findOne()
                                    .where({id: val2[0].current_booking_id})
                                    .exec(function (err, response) {
                                        if (!err)
                                            return res.json(response);
                                        else
                                            return res.json(404, err);
                                    });
                            });
                    }
                }
                else{
                    return res.json(404, err);
                }
            });

    },
    selectedDriver: function (req, res) {
         Driver.findOne({id: req.body.driverId})
            .then(function(driver){
                if(!driver){
                    return res.json("driver no found");
                }
                if (driver.hasOwnProperty('current_booking_id') && driver.current_booking_id != "") {
                    return res.json(404,"driver has already active booking");
                }

                 var Promise = require('q');
                Promise.all([
                    Driver.update({id: req.body.driverId}, {current_booking_id: req.body.bookingId}),
                    Booking.update({id: req.body.bookingId}, {driver: req.body.driverId})
                ]).spread(function(driver , booking){
                    //use the results
                    var retObj = {};
                    retObj.driver = driver;
                    retObj.booking = booking;
                    console.log(retObj);
                    return res.json(retObj);

                }).catch(function(){
                    //handle errors
                    console.log("handle errors");

                }).done(function(){
                    //clean up
                    console.log("clean up");

                });

            });


    },
    cancelBookingByPassenger: function(req, res) {
        Booking.findOne({id: req.body.bookingId})
            .then(function(booking){
                if(!booking)
                    return res.json("booking id not valid");

                //now check passenger id


                var Promises = require('q');
                Promises.all([
                    User.update({id: booking.passenger}, {current_booking_id: ""}),
                    Driver.update({id: booking.driver}, {current_booking_id: ""}),
                    Booking.update({id: booking.id}, {cancelBy: "passenger", is_cancelled: true, is_active: false})
                ]).spread(function(user, driver, booking){
                    var returnObject = {};
                    returnObject.user = user;
                    returnObject.driver = driver;
                    returnObject.booking = booking;
                    return res.json(returnObject.booking[0]);
                }).catch(function(){
                //handle errors
                console.log("handle errors");

            }).done(function(){
                //clean up
                console.log("clean up");

            });

            })
    },
    completeBookingByPassenger: function(req, res) {
        Booking.findOne({id: req.body.bookingId})
            .then(function(booking){
                if(!booking)
                    return res.json("booking id not valid");

                //now check passenger id


                var Promises = require('q');
                Promises.all([
                    User.update({id: booking.passenger}, {current_booking_id: ""}),
                    Driver.update({id: booking.driver}, {current_booking_id: ""}),
                    Booking.update({id: booking.id}, {completeBy: "passenger", is_complete: true, is_active: false})
                ]).spread(function(user, driver, booking){
                    var returnObject = {};
                    returnObject.user = user;
                    returnObject.driver = driver;
                    returnObject.booking = booking;
                    return res.json(returnObject.booking[0]);
                }).catch(function(){
                    //handle errors
                    console.log("handle errors");

                }).done(function(){
                    //clean up
                    console.log("clean up");

                });

            })
    },
    rateBooking: function(req, res) {
        Booking.findOne({id: req.body.bookingId})
            .then(function(booking){
                if(!booking)
                    return res.json("booking id not valid");

                Booking.update({id: req.body.bookingId}, {rating: req.body.rating})
                    .exec(function (err, response) {
                        if (!err)
                            return res.json(response[0]);
                        else
                            return res.json(404, err);
                    });
            })
    },
    cancelBookingByDriver: function(req, res) {
        Booking.findOne({id: req.body.bookingId})
            .then(function(booking){
                if(!booking)
                    return res.json("booking id not valid");

                //now check driver id


                var Promises = require('q');
                Promises.all([
                    User.update({id: booking.passenger}, {current_booking_id: ""}),
                    Driver.update({id: booking.driver}, {current_booking_id: ""}),
                    Booking.update({id: booking.id}, {cancelBy: "driver", is_cancelled: true, is_active: false})
                ]).spread(function(user, driver, booking){
                    var returnObject = {};
                    returnObject.user = user;
                    returnObject.driver = driver;
                    returnObject.booking = booking;
                    return res.json(returnObject.booking[0]);
                }).catch(function(){
                    //handle errors
                    console.log("handle errors");

                }).done(function(){
                    //clean up
                    console.log("clean up");

                });

            })
    },
    completeBookingByDriver: function(req, res) {
        Booking.findOne({id: req.body.bookingId})
            .then(function(booking){
                if(!booking)
                    return res.json("booking id not valid");

                //now check driver id


                var Promises = require('q');
                Promises.all([
                    User.update({id: booking.passenger}, {current_booking_id: ""}),
                    Driver.update({id: booking.driver}, {current_booking_id: ""}),
                    Booking.update({id: booking.id}, {completeBy: "driver", is_complete: true, is_active: false})
                ]).spread(function(user, driver, booking){
                    var returnObject = {};
                    returnObject.user = user;
                    returnObject.driver = driver;
                    returnObject.booking = booking;
                    return res.json(returnObject.booking[0]);
                }).catch(function(){
                    //handle errors
                    console.log("handle errors");

                }).done(function(){
                    //clean up
                    console.log("clean up");

                });

            })
    },
    getById: function(req, res){


        User.findById(req.body.id)
            .exec(function(err, booking){
                return res.json(booking);
            })


    }


};

