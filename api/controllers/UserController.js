/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  // Doing a DELETE /user/:parentid/message/:id will not delete the message itself
  // We do that here.
	remove: function(req, res) {
    var relation = req.options.alias;
    switch (relation) {
      case 'messages':
        destroyMessage(req, res);
    }
  },

  create: function(req, res) {
    res.json(301, 'To create a user go to /auth/register');
  }
};



