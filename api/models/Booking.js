/**
 * Booking.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    attributes: {
        passenger: {
            model: 'user'
        },
        is_complete: {
            type: 'boolean',
            required: true
        },
        is_cancelled: {
            type: 'boolean'
        },
        cancelBy: {
            type: 'string'
        },
        completeBy: {
            type: 'string'
        },
        is_active: {
            type: 'boolean',
            required: true
        },
        pickup_point_lat: {
            type: 'float'
        },
        pickup_point_long: {
            type: 'float'
        },
        pickup_point_name: {
            type: 'string'
        },

        destination_point_lat: {
            type: 'float'
        },
        destination_point_long: {
            type: 'float'
        },
        destination_point_name: {
            type: 'string'
        },
        passenger_note: {
            type: 'string'
        },
        discount: {
            type: 'string'
        },
        ride_type: {
            type: 'text'
        },
        driver: {
            model: 'driver'
        },
        rating: {
            type: 'float'
        }
    }
};

