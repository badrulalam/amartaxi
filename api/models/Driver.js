/**
 * Driver.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
module.exports = {
    schema: true,
    attributes: {
        driverId:{
            type: 'string',
            required: true,
            unique: true
        },
        email: {
            type: 'string',
            unique: true
        },
        name: {
            type: 'string',
            required: true
        },
        mobile: {
            type: 'string',
            required: true,
            unique:true
        },
        car_type: {
            type: 'string'
        },
        current_lat: {
            type: 'float'
        },
        current_long: {
            type: 'float'
        },
        status: {
            type: 'string'
        },
        jid: {
            type: 'string'
            // required: true
        },
        jpassword: {
            type: 'string'
        },
        //positions:{
        //    collection: 'driverposition',
        //    via: 'driverId'
        //},
        current_booking_id: {
            type: 'string',
            defaultsTo: ''
        },
        bookings:{
            collection: 'booking',
            via: 'driver'
        },
        encryptedPassword: {
            type: 'string'
        },
        toJSON: function () {
            var obj = this.toObject();
            delete obj.encryptedPassword;
            return obj;
        }
    },
    beforeCreate: function (values, next) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) return next(err);

            bcrypt.hash(values.password, salt, function (err, hash) {
                if (err) return next(err);

                values.encryptedPassword = hash;
                next();
            });
        });
    },
    validPassword: function (password, driver, cb) {
        bcrypt.compare(password, driver.encryptedPassword, function (err, match) {
            if (err) cb(err);

            if (match) {
                cb(null, true);
            } else {
                cb(err);
            }
        });

    }
};
var bcrypt = require('bcrypt');
